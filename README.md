## magic-user 14 UP1A.231005.007 eng.root.20241025.095833 release-keys
- Manufacturer: qualcomm
- Platform: taro
- Codename: taro
- Brand: qti
- Flavor: magic-user
- Release Version: 14
- Kernel Version: 5.10.198
- Id: UP1A.231005.007
- Incremental: eng.root.20241015.210734
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/taro/taro:12/SKQ1.220119.001/root10152105:user/release-keys
- OTA version: 
- Branch: magic-user-14-UP1A.231005.007-eng.root.20241025.095833-release-keys
- Repo: qti/taro
